import React, { memo } from 'react'

import './meal-item.css'
const component = ({ meal, onSelect, i, selected, isEditing }) => {
  const isSelected = i === selected
  return (
    <div
      className={`${isSelected ? 'selected' : 'meal'}`}
      onClick={() => onSelect(i)}
    >
      {meal.name && (
        <p>
          Name: {isSelected && isEditing ? '*' : ''}
          {meal.name} {isSelected && isEditing ? '*' : ''}
        </p>
      )}
      {meal.price && <p>Price: {meal.price}</p>}
      {meal.quantity && <p>Quantity: {meal.quantity}</p>}
    </div>
  )
}

export const MealItem = memo(component)
