import React from 'react'

import { MealItem } from '../components/meal-item/'

class Meals extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      meals: [
        {
          price: 20.0,
          name: 'Fries',
          quantity: 1
        },

        {
          price: 35.0,
          name: 'Burger',
          quantity: 1
        },

        {
          price: 10.45,
          name: 'Ice cream',
          quantity: 1
        }
      ],
      selected: null,
      name: null,
      price: null,
      quantity: null,
      isEditing: false,
      isCreating: false,
      query: ''
    }
  }

  onSelect = item => {
    const { name, price, quantity } = this.state.meals.filter(
      (_meal, index) => index === item
    )[0]
    this.setState({ selected: item, isCreating: false, name, price, quantity })
  }

  onDeleteMeal = () => {
    const newState = this.state.meals.filter(
      (_m, i) => i !== this.state.selected
    )
    this.setState({ meals: newState })
  }

  onEditMeal = () => {
    const { name, price, quantity } = this.state
    const meals = [...this.state.meals]
    const oldMeal = meals[this.state.selected]
    const newName = name !== null ? name : oldMeal.name
    const newPrice = price !== null ? price : oldMeal.price
    const newQuantity = quantity !== null ? quantity : oldMeal.quantity
    const newMeal = { name: newName, price: newPrice, quantity: newQuantity }
    meals[this.state.selected] = newMeal
    this.setState({
      meals,
      isEditing: false,
      name: null,
      price: null,
      quantity: null
    })
  }

  onCreateMeal = () => {
    const { name, price, quantity } = this.state
    const newMeal = { name, price, quantity }
    this.setState({
      meals: [...this.state.meals, newMeal],
      isCreating: false
    })
  }

  onChangeInput = e => {
    const target = e.target
    const value = target.value
    const name = target.name
    this.setState({
      [name]: value
    })
  }

  toggleEdit = () => {
    this.setState(prevState => {
      return { isEditing: !prevState.isEditing, isCreating: false }
    })
  }

  toggleCreate = () => {
    this.setState(prevState => {
      return {
        isCreating: !prevState.isCreating,
        isEditing: false,
        name: null,
        price: null,
        quantity: null
      }
    })
  }

  renderForm = () => {
    const { isEditing, meals, selected, price, name, quantity } = this.state
    const meal = isEditing && meals.filter((_meal, i) => i === selected)[0]
    return (
      <div>
        <form>
          <input
            type="text"
            name="name"
            value={`${name || meal.name || ''}`}
            placeholder="Name"
            onChange={this.onChangeInput}
          />
          <input
            type="number"
            name="price"
            value={`${price || meal.price || ''}`}
            placeholder="Price"
            onChange={this.onChangeInput}
          />
          <input
            type="number"
            name="quantity"
            value={`${quantity || meal.quantity || ''}`}
            placeholder="Quantity"
            onChange={this.onChangeInput}
          />
          <button
            onClick={isEditing ? this.onEditMeal : this.onCreateMeal}
            type="submit"
          >
            {isEditing ? 'Edit' : 'Create'}
          </button>
        </form>
      </div>
    )
  }

  renderMealsList = () => {
    const { meals, query, selected, isEditing } = this.state

    const filteredMeals = meals
      .filter(meal => meal.name.toLowerCase().includes(query.toLowerCase()))
      .map((meal, i) => {
        return (
          <div key={`meal_${i}`}>
            <MealItem
              meal={meal}
              onSelect={this.onSelect}
              i={i}
              selected={selected}
              isEditing={isEditing}
            />
          </div>
        )
      })
    return filteredMeals.length > 0 ? filteredMeals : <h3>No meals found.</h3>
  }

  render() {
    const { query, selected, isEditing, isCreating } = this.state
    return (
      <>
        <input
          type="text"
          value={query}
          name="query"
          placeholder="Type text to search"
          onChange={this.onChangeInput}
        />
        <button onClick={this.toggleCreate}>Create</button>
        <button onClick={this.toggleEdit} disabled={selected === null}>
          Edit
        </button>
        <button onClick={this.onDeleteMeal} disabled={selected === null}>
          Delete
        </button>
        <div>{(isEditing || isCreating) && this.renderForm()}</div>
        <div>{this.renderMealsList()}</div>
      </>
    )
  }
}

export default Meals
