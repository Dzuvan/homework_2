import React, { Component } from 'react'
import Meals from './containers/meals'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <>
        <Meals/>
        </>
      </div>
    )
  }
}

export default App
